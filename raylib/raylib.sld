(define-library (raylib raylib)
  (import (scheme base))
  (include-shared "raylib")
  (include "constants.scm")
  (include "helpers.scm")

  ;; Core ==============================
  (export 
    init-window
    set-target-fps
    window-should-close
    close-window

    ;; drawing related functions
    clear-background
    begin-drawing
    end-drawing
    begin-mode-2d
    end-mode-2d

    ;; input-related functions -- mouse 
    get-mouse-x
    get-mouse-y
    get-mouse-wheel-move
    get-mouse-position
    is-mouse-button-pressed
    is-mouse-button-down   
    is-mouse-button-release
    is-mouse-button-up     
 
    ;; input-related functions -- keyboard
    is-key-pressed
    is-key-down
    is-key-released
    is-key-up
    set-exit-key
    get-key-pressed
    get-char-pressed)
  
  ;; Shapes ============================
  (export 
    draw-circle 
    draw-circle-v
    draw-rectangle
    draw-rectangle-rec
    draw-rectangle-lines 
    draw-line
    draw-line-ex 
    draw-pixel
    draw-pixel-v)


    ;; Text  =============================
  (export draw-text )

    ;; Structs ===========================
  (export 
    make-color
    color-r  color-g  color-b  color-a
    color-r! color-g! color-b! color-a!
    color!

    make-vector2 
    vector2-x   vector2-y
    vector2-x!  vector2-y!
    vector2!
  
    make-rectangle
    rectangle-x rectangle-y rectangle-width rectangle-height
    rectangle-x! rectangle-y! rectangle-width! rectangle-height!
    
    make-camera2d
    camera2d-offset camera2d-target camera2d-rotation camera2d-zoom
    camera2d-offset! camera2d-target! camera2d-rotation! camera2d-zoom!)

  ;; textures =============================
  (export 
    ;;color/pixel related functions 
    fade)

  ;; misc ================================
  (export set-trace-log-level)

  ;; mouse buttons =======================
  (export  
    MOUSE-BUTTON-LEFT    
    MOUSE-BUTTON-RIGHT   
    MOUSE-BUTTON-MIDDLE)
    
  ;; colors =============================
  (export 
    LIGHTGRAY 
    GRAY      
    DARKGRAY  
    YELLOW    
    GOLD      
    ORANGE    
    PINK      
    RED       
    MAROON    
    GREEN     
    LIME      
    DARKGREEN 
    SKYBLUE   
    BLUE      
    DARKBLUE  
    PURPLE    
    VIOLET    
    DARKPURPLE
    BEIGE     
    BROWN     
    DARKBROWN 
    WHITE     
    BLACK     
    BLANK     
    MAGENTA   
    RAYWHITE)
    
  ;; helpers ======================================
  (export 
    to-int
    to-float) 

  ;; keys  ========================================
  (export 
    KEY-NULL
    KEY-APOSTROPHE
    KEY-COMMA     
    KEY-MINUS     
    KEY-PERIOD    
    KEY-SLASH     
    KEY-ZERO      
    KEY-ONE       
    KEY-TWO       
    KEY-THREE     
    KEY-FOUR      
    KEY-FIVE      
    KEY-SIX       
    KEY-SEVEN     
    KEY-EIGHT     
    KEY-NINE      
    KEY-SEMICOLON 
    KEY-EQUAL     
    KEY-A         
    KEY-B         
    KEY-C         
    KEY-D         
    KEY-E         
    KEY-F         
    KEY-G         
    KEY-H         
    KEY-I         
    KEY-J         
    KEY-K         
    KEY-L         
    KEY-M         
    KEY-N         
    KEY-O         
    KEY-P         
    KEY-Q         
    KEY-R         
    KEY-S         
    KEY-T         
    KEY-U         
    KEY-V         
    KEY-W         
    KEY-X         
    KEY-Y         
    KEY-Z         
    KEY-SPACE         
    KEY-ESCAPE        
    KEY-ENTER         
    KEY-TAB           
    KEY-BACKSPACE     
    KEY-INSERT        
    KEY-DELETE        
    KEY-RIGHT         
    KEY-LEFT          
    KEY-DOWN          
    KEY-UP            
    KEY-PAGE-UP       
    KEY-PAGE-DOWN     
    KEY-HOME          
    KEY-END           
    KEY-CAPS-LOCK     
    KEY-SCROLL-LOCK   
    KEY-NUM-LOCK      
    KEY-PRINT-SCREEN  
    KEY-PAUSE         
    KEY-F1            
    KEY-F2            
    KEY-F3            
    KEY-F4            
    KEY-F5            
    KEY-F6            
    KEY-F7            
    KEY-F8            
    KEY-F9            
    KEY-F10           
    KEY-F11           
    KEY-F12           
    KEY-LEFT-SHIFT    
    KEY-LEFT-CONTROL  
    KEY-LEFT-ALT      
    KEY-LEFT-SUPER    
    KEY-RIGHT-SHIFT   
    KEY-RIGHT-CONTROL 
    KEY-RIGHT-ALT     
    KEY-RIGHT-SUPER   
    KEY-KB-MENU       
    KEY-LEFT-BRACKET  
    KEY-BACKSLASH     
    KEY-RIGHT-BRACKET 
    KEY-GRAVE 
    KEY-KP-0        
    KEY-KP-1        
    KEY-KP-2        
    KEY-KP-3        
    KEY-KP-4        
    KEY-KP-5        
    KEY-KP-6        
    KEY-KP-7        
    KEY-KP-8        
    KEY-KP-9        
    KEY-KP-DECIMAL  
    KEY-KP-DIVIDE   
    KEY-KP-MULTIPLY 
    KEY-KP-SUBTRACT 
    KEY-KP-ADD      
    KEY-KP-ENTER    
    KEY-KP-EQUAL    
    KEY-BACK       
    KEY-MENU       
    KEY-VOLUME-UP  
    KEY-VOLUME-DOWN)
;; log levels =============================================================
  (export 
    LOG-ALL     
    LOG-TRACE   
    LOG-DEBUG   
    LOG-INFO    
    LOG-WARNING 
    LOG-ERROR   
    LOG-FATAL   
    LOG-NONE) )

