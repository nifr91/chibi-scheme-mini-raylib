(define (to-int n)
  (exact (round n)))

(define (to-float n)
  (inexact n))

(define (vector2! v x y)
  (vector2-x! v (to-float x))
  (vector2-y! v (to-float y)))

(define (color! c r g b a)
  (color-r! c (to-int r))
  (color-g! c (to-int g))
  (color-b! c (to-int b))
  (color-a! c (to-int a)))
