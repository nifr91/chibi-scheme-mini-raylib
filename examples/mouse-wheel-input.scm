#!/usr/bin/env chibi-scheme

(import 
  (scheme small) 
  (srfi 159)
  (raylib raylib))

(define (main args) 
  (define screen-width  800)
  (define screen-height 450)
  (define box-position-y (- (/ screen-height 2) 40))
  (define scroll-speed 4)

  (init-window screen-width screen-height "raylib [core] example - input mouse wheel")
  (set-target-fps 60)
  
  (let loop () 
    (if (eq? #f (window-should-close)) 
      (begin 
        (set! box-position-y (- box-position-y (* (get-mouse-wheel-move) scroll-speed)))
        (begin-drawing)
        (clear-background BLACK)
        (draw-rectangle
          (to-int (- (/ screen-width 2)  40))
          (to-int box-position-y)
          80
          80
          MAROON)
        (draw-text "use mouse wheel to move the cube up and down!" 
          10 10 20 GRAY)
        (draw-text 
          (show #f
            (with ((decimal-aling 3)(pad-char #\0))
              (each 
                "Box position Y: "
              (padded 3 (numeric (to-int box-position-y))))))
          10 30 20 LIGHTGRAY)

        (end-drawing)
        (loop))
      (close-window))))

(main (cdr (command-line)))
