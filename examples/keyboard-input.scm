#!/usr/bin/env chibi-scheme 

(import 
  (chibi)
  (raylib raylib))

(define (main args) 
  (define screen-width  800)
  (define screen-height 450)
  (define ball-position (make-vector2 (/ screen-width 2.0) (/ screen-height 2.0)))
     
  (init-window screen-width screen-height "raylib [core] example - input mouse wheel")
  (set-target-fps 60)
  
  (let loop () 
    (if (eq? #f (window-should-close)) 
      (begin 
        (if (is-key-down KEY-RIGHT) (vector2-x! ball-position (+ (vector2-x ball-position) 2.0)))
        (if (is-key-down KEY-LEFT)  (vector2-x! ball-position (- (vector2-x ball-position) 2.0)))
        (if (is-key-down KEY-UP)    (vector2-y! ball-position (- (vector2-y ball-position) 2.0)))
        (if (is-key-down KEY-DOWN)  (vector2-y! ball-position (+ (vector2-y ball-position) 2.0)))
        
        (begin-drawing)
        (clear-background BLACK)
        (draw-text "move the ball with arrow keys" 10 10 20 GRAY)
        (draw-circle-v ball-position 50.0 MAROON)
        (end-drawing)

        (loop))
      (close-window))))

(main (cdr (command-line)))
