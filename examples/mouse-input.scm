#!/usr/bin/env chibi-scheme

(import 
  (scheme small) 
  (scheme write)
  (raylib raylib))

(define (main args) 
  (define screen-width  800)
  (define screen-height 450)
  (define bpos (make-vector2 -100.0 -100.0))

  (init-window screen-width screen-height "raylib [core] example - mouse input")
  (define bcolor  DARKBLUE)
  (set-target-fps 60)
  
  (let loop () 
    (if (eq? #f (window-should-close)) 
      (begin 
        (set! bpos (get-mouse-position))
        (cond 
          ((is-mouse-button-pressed MOUSE-BUTTON-LEFT)    (set! bcolor MAROON))
          ((is-mouse-button-pressed MOUSE-BUTTON-MIDDLE)  (set! bcolor LIME))
          ((is-mouse-button-pressed MOUSE-BUTTON-RIGHT)   (set! bcolor DARKBLUE)))

        (begin-drawing)
        (clear-background BLACK)
        (draw-circle-v bpos 40.0 bcolor)
        (draw-text "move ball wih mouse and click mouse button to change color" 10 10 20 GRAY)
        (end-drawing)
        (loop))
      (close-window))))

(main (cdr (command-line)))
