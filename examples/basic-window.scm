#!/usr/bin/env chibi-scheme

(import 
  (scheme small) 
  (scheme write)
  (raylib raylib))

(define (main args) 
  (define white (make-color 0 0 0 255))
  (define lightgray (make-color 200 200 200 255))

  (init-window 800 400 "eplot")
  (set-target-fps 60)
  (let loop () 
    (if (eq? #f (window-should-close)) 
      (begin 
        (begin-drawing)
        (clear-background white)
        (draw-text "Congrats! You created your fist window"
          190 200 20 lightgray)
        (end-drawing)
        (loop))
      (close-window))))

(main (cdr (command-line)))
